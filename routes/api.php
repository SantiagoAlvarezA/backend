<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
require __DIR__ . '../../app/Http/Controllers/Prueba/RoutesPrueba.php';
require __DIR__ . '../../app/Http/Controllers/Auth/RoutesAuthController.php';


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user()['last_name'];
});


Route::middleware('rol:admin', 'auth:api')->get('/ruta', function () {
    return 'return middleware';
});

Route::get('/', function () {
    return response()->json([
        'msg' => 'Esta aplicación fue creada para la empresa multialarmas del sur en la ciudad de pasto',
        'Autores' => [
            'Santiago Alvarez Alvarez', 'Christian Camilo Angulo Mirama', 'Jorge Alexander Angulo Mirama'
        ]
    ], 200);
});
