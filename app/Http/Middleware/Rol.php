<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use http\Env\Request;
use phpDocumentor\Reflection\Types\Boolean;

class Rol
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $rol)
    {
        if (!$this->hasRol($request, $rol)) {
            abort(403, "No tienes autorización para ingresar.");
        }

        return $next($request);
    }

    public function hasRol($request, $rol)
    {
        $auth_rol = false;
        $rol_user = User::join('rol', 'rol.id', '=', 'users.id_rol')
            ->select('rol.desc_rol as rol')
            ->where('document', $request->user()['document'])
            ->first();

        if ($rol == $rol_user['rol']) {
            $auth_rol = true;
        }


        return $auth_rol;
    }
}
