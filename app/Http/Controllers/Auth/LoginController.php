<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {

        $data = null;
        $msg = null;
        $status = null;

        $credentials = $request->only('document', 'password');
        if (Auth::attempt($credentials)) {

            $msg = "Bienvenido a multialarmas del sur";

            $data = User::join('rol', 'rol.id', '=', 'users.id_rol')
                ->select('users.*', 'rol.desc_rol as rol')
                ->where('document', $request->only('document'))
                ->first();
            $status = 302;

        } else {
            $msg = "Documento y/o contraseña  incorrectos";
            $status = 404;
        }

        return response()->json([
            'msg' => $msg,
            'data' => $data
        ], $status);

    }

    public function logout()
    {
        Auth::logout();
        return response()->json([
            'msg' => "Se a cerrado sesión con exito",
        ], 200);

    }

}
