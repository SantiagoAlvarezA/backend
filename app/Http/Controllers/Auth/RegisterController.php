<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
        return $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'document' => ['required', 'string', 'max:20', 'unique:users'],
            'phone' => ['required', 'string', 'max:20'],
            'id_rol' => ['required'],
            'address' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $data = null;
        $msg = null;
        $status = null;

        $data2 = null;

        if ($this->validator($request)->fails()) {
            $msg = "Formulario de registro de usuario invalido";
            $data = $this->validator($request)->errors();
            $status = 400;
        } else {
            $user = User::create([
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'document' => $request['document'],
                'phone' => $request['phone'],
                'address' => $request['address'],
                'id_rol' => $request['id_rol'],
                'password' => Hash::make($request['password']),
                'api_token' => Str::random(64),
                'remember_token' => Str::random(10),
            ]);
            if ($user) {
                $msg = "Usuario registrado con exito";
                $data = $user;
                $status = 201;

                $credentials = $request->only('document', 'password');
                $data2 = Auth::attempt($credentials);

            }
        }

        return response()->json([
            "data2" => $data2,
            'msg' => $msg,
            'data' => $data
        ], $status);
    }
}
