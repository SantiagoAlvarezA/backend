<?php

Route::group(['middleware' => ['cors:api']], function () {
    Route::post('user/login', 'Auth\LoginController@authenticate');
    Route::post('user/register', 'Auth\RegisterController@create');
    Route::get('user/logout', 'Auth\LoginController@logout');
});


