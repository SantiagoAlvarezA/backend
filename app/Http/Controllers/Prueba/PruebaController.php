<?php

namespace App\Http\Controllers\Prueba;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class PruebaController extends Controller
{

    public function index()
    {
        return response()->json([
            "msg" => 'index',
            "data" => [1,2,3,4,5,6]
        ], 200);
    }

    public function store(Request $request)
    {

        return response()->json(
            [
                "msg" => 'store',
                "data" => ['store','store',  $request['api_token'], $request['id']]
            ]);
    }

    public function update(Request $request, $id)
    {
        return response()->json([
            "msg" => 'update',
            "data" => ['update',123,12,1]
        ]);
    }

    public function show($id)
    {

        return response()->json([
            "msg" => 'show',
            "data" =>[12,12,123,1234, $id]
        ]);
    }

    public function destroy($id)
    {

        return response()->json([
            "msg" => 'destroy',
            "data" => [0,1,2,3,4]
        ]);
    }

}
